#ifndef MOCK_MAKER_H_
#define MOCK_MAKER_H_

#include "indent.h"
#include "match_action.h"

#include <map>
#include <vector>
#include <string>

namespace mm
{
    class MockMaker final :
        public MatchAction<clang::ast_matchers::DeclarationMatcher>
    {
    public:
        void run(const clang::ast_matchers::MatchFinder::MatchResult& res)
            override;

        void onEndOfTranslationUnit();

        MatchResult createMatcher() const override;

    private:
        Indent indent;
        std::map<std::string, std::vector<const clang::CXXMethodDecl*>> classDecls;

        void dumpMockClass(clang::raw_ostream& os,
                const std::string& className,
                const std::vector<const clang::CXXMethodDecl*>& decls);

        void dumpMockMember(clang::raw_ostream& os,
                const clang::CXXMethodDecl& decl);
    };

} /* namespace mm */

#endif /* MOCK_MAKER_H_ */
