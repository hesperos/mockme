#ifndef INDENT_H_
#define INDENT_H_

#include <memory>
#include <string>

namespace mm
{
    class Indent
    {
    public:
        enum class Type
        {
            Space,
            Tab
        };

        Indent(Type type = Type::Space, std::size_t size = 4);

        Indent& operator++();
        Indent& operator--();

        std::string echo() const;
        std::size_t getCurrent() const;

    private:
        Type type;
        std::size_t size;
        std::size_t current;
    };

    class ScopedIndent
    {
    public:
        ~ScopedIndent();
        ScopedIndent(Indent& indent);

    private:
        Indent& indent;
    };
} /* namespace mm */


#endif /* INDENT_H_ */
