#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "../indent.h"

class IndentTest :
    public ::testing::Test
{
public:
    void SetUp()
    {
    }

    void TearDown()
    {
    }
};

TEST_F(IndentTest, test_ifDefaultConstructionWorks)
{
    mm::Indent indent{};
}

TEST_F(IndentTest, test_ifIncrementsDecrements)
{
    mm::Indent indent{};
    ASSERT_EQ(0, indent.getCurrent());
    ASSERT_EQ(1, (++indent).getCurrent());
    ASSERT_EQ(2, (++indent).getCurrent());
    ASSERT_EQ(3, (++indent).getCurrent());

    ASSERT_EQ(2, (--indent).getCurrent());
    ASSERT_EQ(1, (--indent).getCurrent());
    ASSERT_EQ(0, (--indent).getCurrent());
    ASSERT_EQ(0, (--indent).getCurrent());
}

TEST_F(IndentTest, test_IfScopedIndentWorks)
{
    mm::Indent indent{};
    {
        mm::ScopedIndent si1{indent};
        ASSERT_EQ(1, indent.getCurrent());
        {
            mm::ScopedIndent si2{indent};
            ASSERT_EQ(2, indent.getCurrent());
            {
                mm::ScopedIndent si3{indent};
                ASSERT_EQ(3, indent.getCurrent());
            }
            ASSERT_EQ(2, indent.getCurrent());
        }
        ASSERT_EQ(1, indent.getCurrent());
    }
    ASSERT_EQ(0, indent.getCurrent());
}

TEST_F(IndentTest, test_ifOutputIsCorrect)
{
    mm::Indent si{mm::Indent::Type::Space, 10};
    mm::Indent ti{mm::Indent::Type::Tab, 1};

    ASSERT_EQ(0, si.echo().length());
    ASSERT_EQ(10, (++si).echo().length());
    ASSERT_EQ(20, (++si).echo().length());
    ASSERT_EQ(std::string(20,' '), si.echo());

    ASSERT_EQ(0, ti.echo().length());
    ASSERT_EQ(1, (++ti).echo().length());
    ASSERT_EQ(2, (++ti).echo().length());
    ASSERT_EQ(std::string(2,'\t'), ti.echo());
}
