#ifndef SIMPLE_H_
#define SIMPLE_H_

#include <string>

namespace test
{
    class SimpleInterface
    {
    public:
        virtual ~SimpleInterface() = default;

        virtual void foo() const = 0;

        virtual std::string bar(int, const std::string&, void*) = 0;

        virtual const int& baz(int, void*) const = 0;

        virtual const int& bax(int, double, const char*);

        void this_is_not_an_interface();

        static void this_is_a_static(int);

    private:
        int some_data;
    };
} /* namespace test */

#endif /* SIMPLE_H_ */
