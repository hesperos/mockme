#include "mmtool.h"

namespace mm
{
    MmTool::MmTool(int argc, const char* argv[]) :
        toolCategory("clang based gmock maker"),
        optionParser(argc, argv, toolCategory),
        tool(optionParser.getCompilations(),
                optionParser.getSourcePathList())
    {
    }

    std::map<std::string, clang::tooling::Replacements>&
    MmTool::getReplacements()
    {
        return tool.getReplacements();
    }

    int MmTool::run(
            std::unique_ptr<clang::tooling::FrontendActionFactory> actionFactory)
    {
        return tool.runAndSave(actionFactory.get());
    }
 
} /* namespace mm */
