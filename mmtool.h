#ifndef MMTOOL_H_
#define MMTOOL_H_

#include <clang/ASTMatchers/ASTMatchFinder.h>
#include <clang/Tooling/CommonOptionsParser.h>
#include <clang/Tooling/Refactoring.h>
#include <clang/Tooling/Tooling.h>
#include <llvm/ADT/StringRef.h>
#include <llvm/Support/CommandLine.h>

#include <memory>

namespace mm
{
    class MmTool
    {
    public:
        MmTool(int argc, const char* argv[]);

        std::map<std::string, clang::tooling::Replacements>&
            getReplacements();

        int run(std::unique_ptr<clang::tooling::FrontendActionFactory> actionFactory);

    protected:
        llvm::cl::OptionCategory toolCategory;
        clang::tooling::CommonOptionsParser optionParser;
        clang::tooling::RefactoringTool tool;
    };
} /* namespace mm */

#endif /* MMTOOL_H_ */
