#!/bin/bash

mkdir -p bld
(cd bld && cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=Debug ..)
make -C bld
