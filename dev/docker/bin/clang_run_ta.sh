#!/bin/bash

# Run clang-tool on a test asset
declare -r tool="${1:-clang-check}"
declare -r asset="${2:-}"
shift 2

"${tool}" "$@" "test/assets/${asset}" -- \
    $(pkg-config --libs --cflags cppunit) \
    -fdiagnostics-color=never
