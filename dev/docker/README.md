# Docker containers configuration

This directory contains some docker files for various containers useful during
the development.

## Containers

### Development container

Container for building and development of mockme. Typical usage.

Building the container:

    docker build -t mm-dev -f dev/docker/Dockerfile-mm dev/docker

Building with the container:

    docker run -v $PWD:/mnt -it mm-dev

### Packaged application
