#!/bin/bash

# helper for running gtm-dev container
docker run --rm -v $PWD:/mnt --privileged -it mm-dev "$@"
