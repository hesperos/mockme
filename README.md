[![pipeline status](https://gitlab.com/hesperos/mockme/badges/master/pipeline.svg)](https://gitlab.com/hesperos/mockme/commits/master)
# mockme

`mockme` is a clang based gmock generator. It scans for virtual member
functions in the provided code base and outputs mock objects.

## Usage

`mockme` is a compiler based tool operating on a C++ AST. In order to be
able to build the AST it must be able to perform some stages of code
compilation. That means that, the same way as for the compiler, the
proper environment is required.

    $ mm-tool-generatemock my_translation_unit.cpp -- -Imy_includes -Lmy_libs ... <other compilation flags>

# TODO

- [ ] support for namespaces
- [ ] support for command-line flags
- [ ] conditional recursive base members generation
- [ ] support for multiple bases (mock object parents)
- [ ] mock generation only for selected class names
