#ifndef MATCH_ACTION_H_
#define MATCH_ACTION_H_

#include <clang/ASTMatchers/ASTMatchers.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>
#include <clang/Tooling/Refactoring.h>

namespace mm
{
    template <typename MatchResultT>
        class MatchAction :
            public clang::ast_matchers::MatchFinder::MatchCallback
    {
    public:
        using MatchResult = MatchResultT;

        virtual ~MatchAction() = default;

        virtual MatchResultT createMatcher() const = 0;
    };

} /* namespace mm */

#endif /* MATCH_ACTION_H_ */
