#include "indent.h"

namespace mm
{
    Indent::Indent(Type type, std::size_t size) :
        type(type),
        size(size),
        current(0)
    {
    }

    Indent& Indent::operator++()
    {
        current++;
        return *this;
    }

    Indent& Indent::operator--()
    {
        if (current > 0)
        {
            current--;
        }
        return *this;
    }

    std::size_t Indent::getCurrent() const
    {
        return current;
    }

    std::string Indent::echo() const
    {
        return std::string(size * current,
                type == Type::Space ? ' ' : '\t');
    }

    ScopedIndent::~ScopedIndent()
    {
        --indent;
    }

    ScopedIndent::ScopedIndent(Indent& indent) :
        indent(indent)
    {
        ++indent;
    }
} /* namespace mm */
