#include "mmtool.h"
#include "mock_maker.h"

int main(int argc, const char *argv[])
{
    mm::MmTool tool{argc, argv};
	clang::ast_matchers::MatchFinder finder;
	mm::MockMaker mockMaker{};
	finder.addMatcher(mockMaker.createMatcher(), &mockMaker);

	int rv = tool.run(
			clang::tooling::newFrontendActionFactory(&finder));

	return rv;
}
