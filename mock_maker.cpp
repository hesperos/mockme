#include "mock_maker.h"

#include <sstream>

#include <boost/algorithm/string/join.hpp>

namespace
{
    inline std::string makeMockClassName(std::string className)
    {
        return "Mock" + className;
    }
} /* namespace  */

namespace mm
{
    void MockMaker::run(
            const clang::ast_matchers::MatchFinder::MatchResult& res)
    {
        const clang::CXXMethodDecl* decl =
            res.Nodes.getNodeAs<clang::CXXMethodDecl>("cxxMockable");

        if (nullptr == decl)
        {
            llvm::errs() << "Unable to extract node\n";
            return;
        }

        const clang::CXXRecordDecl* declParent = decl->getParent();
        if (!declParent)
        {
            llvm::errs() << "Can't find member function's parent\n";
            return;
        }

        const std::string parentName = declParent->getNameAsString();
        classDecls[parentName].push_back(decl);
    }

    void MockMaker::dumpMockMember(clang::raw_ostream& os,
            const clang::CXXMethodDecl& decl)
    {
        const size_t nArgs = decl.getNumParams();
        const std::string funcName = decl.getNameAsString();
        const std::string returnType = decl.getReturnType().getAsString();
        const bool isConst = decl.isConst();
        const std::vector<std::string> args = [decl]() {
            std::vector<std::string> tmp;
            std::transform(decl.param_begin(),
                    decl.param_end(),
                    std::back_inserter(tmp),
                    [](const auto p) { return p->getOriginalType().getAsString(); });
            return std::move(tmp);
        }();

        os << indent.echo()
            << "MOCK_"
            << (isConst ? "CONST_" : "")
            << "METHOD"
            << nArgs
            << "(" << funcName << ", "
            << returnType
            << "(" << boost::algorithm::join(args, ", ") << ")"
            << ");";
    }

    void MockMaker::dumpMockClass(clang::raw_ostream& os,
            const std::string& className,
            const std::vector<const clang::CXXMethodDecl*>& decls)
    {
        os << indent.echo()
            << "class " << makeMockClassName(className) << " :\n";
        {
            ScopedIndent si{indent};
            os << indent.echo()
                << "public " << className << "\n";
        }

        os << indent.echo() << "{\n";
        os << indent.echo() << "public:\n";

        {
            ScopedIndent si{indent};
            for (const auto d : decls)
            {
                dumpMockMember(os, *d);
                os << "\n";
            }
        }

        os << indent.echo() << "};\n";
    }

    void MockMaker::onEndOfTranslationUnit()
    {
        for (const auto c : classDecls)
        {
            dumpMockClass(llvm::outs(),
                    c.first,
                    c.second);
            llvm::outs()  << "\n";
        }
    }

    MockMaker::MatchResult MockMaker::createMatcher() const
    {
        using namespace clang::ast_matchers;
        return cxxMethodDecl(
                allOf(isVirtual(),
                    unless(anyOf(
                            isExpansionInSystemHeader(),
                            cxxDestructorDecl(),
                            isDefinition())))).bind("cxxMockable");
    }
} /* namespace mm */
